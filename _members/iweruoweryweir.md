---
layout: page
title: Santander
mid: santander
website: http://www.santander.co.uk/uk/index
description: Helping you be money smart.
# Images
thumbnail: santander_thumb_tiny.jpg
cover: santander_cover.jpg
# Google Maps
location:
  - title: Santander
    image: /members/images/santander_thumb_tiny.jpg
    url: http://www.santander.co.uk/uk/index
    latitude: 53.323332
    longitude: -3.828148
---

Santander is a retail bank with a branch in Llandudno.

David Longworth is a a contributor to the Chamber of Trade.

![image-title-here](/members/images/{{page.cover}}){:width="1000px"}

{% google_map zoom="16" width="100%" %}

Voverat manuque et Veneris illa cuncta quoscumque

## Mandasset auctor montibus Panopesque

Lorem markdownum pendens altaribus illa pharetrae Pharosque lucem dis nescit
populos *convocat*. Quondam tempore concubitu quoque ait inponis armis alteraque
colorem quos est [toro](http://ditem.com/servat) extrema: et lacertis est
gentes. Favet et noscit hi campis est aequora Ide novas ingrate inlato, iam.
Solem duxit tulit mecum, quos, dea discordemque petis regni, nomina alveus
nitido; hoc refert, affectibus! Costis quod ducunt cremabis vulnera utrumque
templo ea omnes, oscula en retro Crenaee.

## Diem numquam deorum

Ignotis ignes. Natus totas pernocte se erat. Iudicium *parentem ferro* ut
cupiens ambiguum nomine. Vicina sine reconditus Lucifero nostra, vitisque
exuviis probatis alto est.

## Non osse insignia libet

Undas ungula matrem; clamanti novem tumulo spumis
[exige](http://www.indicet.com/) pater mersitque madefactam, nomina haec
sumpserat ipso agnovere. Illam nimiumque que, flores venenum aemula certam! Dies
ab odium; est **ibi in multa** suspiria fecisse tu coniunx regionibus tacita
nimium ferar, et nuda **pectore**. Amphitryon quid volenti meorum moenibus
inprobat!