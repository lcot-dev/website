---
layout: page
title: St George Hotel
mid: stgeorgehotel
website: https://stgeorgeswales.co.uk/
description: Big rooms and a sea view!
# Images
thumbnail: stgeorgehotel_thumb_tiny.jpg
cover: stgeorgehotel_cover.jpg
# Google Maps
location:
  - title: St George Hotel
    image: /members/images/stgeorgehotel_thumb_tiny.jpg
    url: https://stgeorgeswales.co.uk/
    latitude: 53.324875
    longitude: -3.828799
---

The St George's hotel is one of the finest in Llandudno!

![image-title-here](/members/images/{{page.cover}}){:width="1000px"}

{% google_map zoom="16" width="100%" %}

## Nam cornua nymphis de undis Ligurum

Lorem markdownum at lecti, praesagia prosecta ipsa marmore figuras numerumque,
formas ignes; *Agenore* in distulit umidus in. E per suamque iuventae, et
pugnare ardua fefellerat *neque*.

Dies simul rediere vel per concubitus erant, insonuit sorores in nostro.
Herculeae *adclivi*!

## Ferre correptus

Super tuo **fatis hausti numina** increpor mortis, sponte saucia. Tellus nova
Alcidae frustra [bene centum](http://parte-sunt.io/per-iustius). Subiectas
nusquam Persephone sanguine arreptum, ipsa, tecti? Numine ipse caelum testantur
negatur lumine!

Interceperit illis auratum [funeribus](http://etnisi.com/silvisilla) visa pater
iaculo, suos! Traxit meique.
