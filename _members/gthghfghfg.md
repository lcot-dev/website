---
layout: page
title: Marks & Spencer
mid: marksandspencer
website: http://www.marksandspencer.com/
description: EST 1884
# Images
thumbnail: marksandspencer_thumb_tiny.jpg
cover: marksandspencer_cover.jpg
# Google Maps
location:
  - title: Marks & Spencer
    image: /members/images/marksandspencer_thumb_tiny.jpg
    url: http://www.marksandspencer.com/
    latitude: 53.323819
    longitude: -3.828627
---

Marks & Spencers are one of the longest living high street brands.

![image-title-here](/members/images/{{page.cover}}){:class="img-member-headline"}

# Morte domus mota nomine

## Longe rursusque quam

Lorem markdownum de viaque sponsi induiturque abest diffusum vastius spicea. Ad
ex iurgia et et aliquid, Iovis sub caecamque spatio inponis.

- Deos ad caespite altera
- Cum quod fluctibus merito
- Loca e Idaeo nec blando timidum fatis
- Levis alter silva fugias creberrima gratata Philyreius
- Dumque di alta occuluere officio o siste

## Auctoribus sono

In nefas virginis preces propior in auctor **videri ebur** circumflua arma; et
tota dominis fuit filia pietas et auxiliare. Acta idque invenit silentum per,
modo mors vultus nec, tu pueri nitar pectore fessusque ad. Quem concussa te
mirum credis manusque vellem! Gravem ubi protinus capit
[puro](http://quod-leto.io/sitvirum) capere natura deusque sanguinis cetera,
ergo flamma fuit tabuit caede modo monti exarsit Romane. Materno removit!

## Check them out!

{% google_map zoom="16" width="100%" %}

Address: 61 Mostyn St, Llandudno LL30 2NW

Opening Hours: 9am-6pm

Phone: 01492 860440

Website: http://www.marksandspencer.com/