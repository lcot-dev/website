---
layout: page
title: Join
permalink: /join/
---

Becoming a member of the Llandudno Chamber of Trade has many benefits such as, 

* **Access to a exclusive forum**
* **Inclusion in the Chamber of Trade's promotional campaigns**

If this sounds good to you, please fill in the form below and checkout. We would be very happy to have you!

We aim to keep the membership fee as low as possible while still leaving enough money to cover operational costs that we use to campaign and serve you better.

**£12.99** for 6 Months  
**£23.38** for 12 Months 

Your membership will renew automatically.

{% include joinform.html %}