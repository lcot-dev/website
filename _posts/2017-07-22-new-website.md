---
layout: post
title:  "New website!"
date:   2017-07-22 20:00:00
categories: chamber update website
---
Welcome to our new website!

As i'm sure you have noticed by now we have a new layout and new features thanks to our new volunteer developers.

They have added new features such as,

* Easy contact forms
* Members forum
* High resolution images
* Compatibility with mobile devices

Thank you for making this all possible by putting faith in us and becoming a member.

Wish you all the best!

