# Llandudno Chamber of Trade
Development repository for the Llanduno Chamber of Trade website.

This is a static site to be build with the static file CMS Jekyll.

Required Ruby GEM dependencies are:

	- Jekyll Maps
	
	These can be automatically installed with "bundle install"

Resources used include:

[Simple Grid](http://simplegrid.io)
	
Development site is available to view [here.](http://lcot.dev.auriga.tech)